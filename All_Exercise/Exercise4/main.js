const nameinput = document.querySelector('.form-control-name')
const cardinput = document.querySelector('.form-control-number')
const monthinput = document.querySelector('.form-control-mm')
const yearinput = document.querySelector('.form-control-yy')
const cvcinput = document.querySelector('.form-control-cvc')

const cardname = document.querySelector('.card-name')
const cardnumber = document.querySelector('.card-number')
const cardmonth = document.querySelector('.month')
const cardyear = document.querySelector('.year')
const cardcvc = document.querySelector('.cvc')

const groupName = document.querySelector('.form-group')
nameinput.oninput = function () {
    if (nameinput.value === '') {
        groupName.classList.add('invalid')
    } else {
        groupName.classList.remove('invalid')
        cardname.innerText = nameinput.value
    }
}


const groupNumber = document.querySelector('.form-group-number')
cardinput.oninput = function () {
    if (isNaN(cardinput.value)) {
        groupNumber.classList.add('invalid')
        groupNumber.classList.remove('error')
    } else if (cardinput.value === '') {
        groupNumber.classList.remove('invalid')
        groupNumber.classList.add('error')
    } else {
        groupNumber.classList.remove('invalid')
        groupNumber.classList.remove('error')
        cardnumber.innerText = cardinput.value
    }
}


const groupM = document.querySelector('.form-group-mm')
monthinput.oninput = function () {
    if (isNaN(monthinput.value) || monthinput.value > 30 ) {
        groupM.classList.add('invalid')
        groupM.classList.remove('error')
    } else if (monthinput.value === '') {
        groupM.classList.add('error')
        groupM.classList.remove('invalid')
    } else {
        groupM.classList.remove('invalid')
        groupM.classList.remove('error')
        cardmonth.innerText = monthinput.value
    }
}


const groupY = document.querySelector('.form-group-yy')
yearinput.oninput = function () {
    if (isNaN(yearinput.value)) {
        groupY.classList.add('invalid')
        groupY.classList.remove('error')
    } else if (yearinput.value === '') {
        groupY.classList.remove('invalid')
        groupY.classList.add('error')
    } else {
        groupY.classList.remove('invalid')
        groupY.classList.remove('error')
        cardyear.innerText = yearinput.value
    }
}

const groupCvc = document.querySelector('.form-group-cvc')
cvcinput.oninput = function () {
    if (isNaN(cvcinput.value)) {
        groupCvc.classList.add('invalid')
        groupCvc.classList.remove('error')
    } else if (cvcinput.value === '') {
        groupCvc.classList.remove('invalid')
        groupCvc.classList.add('error')
    } else {
        groupCvc.classList.remove('invalid')
        groupCvc.classList.remove('error')
        cardcvc.innerText = cvcinput.value
    }
}

const submit = document.querySelector('.form-submit')
const form = document.querySelector('.form')
const sucess = document.querySelector('.container-sucessfull')
submit.onclick = function () {
    if (nameinput.value === '') {
        groupName.classList.add('invalid')
    } else if (cardinput.value === '') {
        groupNumber.classList.add('error')
    } else if (monthinput.value === '') {
        groupM.classList.add('error')
    } else if (yearinput.value === '') {
        groupY.classList.add('error')
    } else if (cvcinput.value === '') {
        groupCvc.classList.add('error')
    }else {
        form.classList.add('hide')
        sucess.classList.add('show')
    }
}

const btn = document.querySelector('.btn-continue')
btn.onclick = function () {
    form.classList.remove('hide')
    sucess.classList.remove('show')
}