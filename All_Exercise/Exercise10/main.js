const iconDarkmod = document.querySelector('.toggle-btn')
const card = document.querySelectorAll('.card')
const miniCard = document.querySelectorAll('.minicard')
const text = document.querySelector('.text')
const overview = document.querySelector('.overview__today')
const toggleBtn = document.querySelector('.toggle-btn')

iconDarkmod.onclick = function() {
    toggleBtn.classList.toggle('active')
    document.body.classList.toggle('dark')
    document.body.classList.toggle('whitecolor')
    card.forEach(function(item){
        item.classList.toggle('darkcard')
    })
    miniCard.forEach(function(item){
        item.classList.toggle('darkcard')
    })
    text.classList.toggle('whitecolor')
    overview.classList.toggle('whitecolor')
}

