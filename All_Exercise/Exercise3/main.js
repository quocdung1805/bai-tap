const icon = document.querySelector('.icon')
const share = document.querySelector('.share')
icon.onclick = function() {
    share.classList.toggle('active')
    if (share.classList.contains('active')) {
        icon.classList.add('change')
    } else {
        icon.classList.remove('change')
    }
}