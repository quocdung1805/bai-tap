// function init() {
//     fetch('http://localhost:3000/data')
//         .then((response) => response.json())
//         .then((data) => {
//             drawChart(data);
//         });
            
// }   

// init();

const data = [
  {
    id: 1,
    day: 'mon',
    amount: 17.45
  },
  {
    id: 2,
    day: 'tue',
    amount: 34.91
  },
  {
    id: 3,
    day: 'wed',
    amount: 52.36
  },
  {
    id: 4,
    day: 'thu',
    amount: 31.07
  },
  {
    id: 5,
    day: 'fri',
    amount: 23.39
  },
  {
    id: 6,
    day:'sat',
    amount: 43.28
  },
  {
    id: 7,
    day: 'sun',
    amount: 25.48
  }
]

function drawChart(data) {
    const ctx = document.getElementById('myChart');
    const labels = data.map((chart) => chart.day);
    const datasets = data.map((chart) => chart.amount);

    new Chart(ctx, {
        type: 'bar',
        data: {
          labels: labels,
          datasets: [{
            label: '',
            data: datasets,
            borderWidth: 1,
            borderRadius: 4,
            backgroundColor: 'rgb(245, 95, 72)',
            hoverBackgroundColor: 'rgb(245, 95, 72, 0.8)'
          }]
        },

        options: {
          scales: {
            y: {
                display:false,
                
            },
          }          
        },
      });
}

drawChart(data);
