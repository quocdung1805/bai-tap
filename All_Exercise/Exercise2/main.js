const container = document.querySelectorAll('.container')

container.forEach(function(item) {
    const questionarrow = item.querySelector('.question-arrow')
    questionarrow.onclick = function () {
        item.classList.toggle('active')
        const subtitle = item.querySelector('.subtitle')
        if (item.classList.contains('active')) {
            subtitle.classList.add('open')
            item.querySelector('i').classList.replace('fa-chevron-down', 'fa-chevron-up')
        } else {
            subtitle.classList.remove('open')
            item.querySelector('i').classList.replace('fa-chevron-up', 'fa-chevron-down')
        }
    } 
})

