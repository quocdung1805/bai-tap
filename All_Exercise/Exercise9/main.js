const Features = document.querySelector('.features')
const Company = document.querySelector('.company')
const FeaturesList = document.querySelector('.features-list')
const CompanyList = document.querySelector('.company-list')
const iconWeb = document.querySelector('.icon')
const icon = document.querySelector('.icon-menu')
const iconMoblieClose = document.querySelector('.icon-moblie')
const mobile = document.querySelector('.mobile')
const overlay = document.querySelector('.overlay')
const body = document.querySelector('body')
const featureMobile = document.querySelector('.feature__list-moblie')
const companyMobile = document.querySelector('.company__list-moblie')

Features.onclick = function() {
    FeaturesList.classList.toggle('show')
    if(FeaturesList.classList.contains('show')) {
        document.querySelector('i').classList.replace('fa-chevron-down', 'fa-chevron-up')
    } else {
        document.querySelector('i').classList.replace('fa-chevron-up', 'fa-chevron-down')
    }
}


Company.onclick = function() {
    CompanyList.classList.toggle('show')
    if(CompanyList.classList.contains('show')) {
        iconWeb.classList.replace('fa-chevron-down', 'fa-chevron-up')
    } else {
        iconWeb.classList.replace('fa-chevron-up', 'fa-chevron-down')
    }
}

icon.onclick = function() {
    mobile.classList.add('show')
    body.classList.add('overlay')
}

iconMoblieClose.onclick = function() {
    mobile.classList.remove('show')
    body.classList.remove('overlay')
}

const feature = document.querySelector('.features-mobile')
feature.onclick = function() {
    featureMobile.classList.toggle('show')
    const iconTop = document.querySelector('.icon-top')
    if (featureMobile.classList.contains('show')) {
        iconTop.classList.replace('fa-chevron-down', 'fa-chevron-up')
    } else {
        iconTop.classList.replace('fa-chevron-up', 'fa-chevron-down')
    }
}

const company = document.querySelector('.company-moblie')
company.onclick = function () {
    companyMobile.classList.toggle('show')
    const iconBot = document.querySelector('.icon-bot')
    if (companyMobile.classList.contains('show')) {
        iconBot.classList.replace('fa-chevron-down', 'fa-chevron-up')
    } else {
        iconBot.classList.replace('fa-chevron-up', 'fa-chevron-down')
    }
}