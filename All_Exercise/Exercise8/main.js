const openMenu = document.querySelector('.icon')
const closeMenu = document.querySelector('.close')
const navMobile = document.querySelector('.nav-mobile')
const overlay = document.querySelector('.overlay')

openMenu.onclick = function() {
    navMobile.classList.add('show')
    overlay.classList.add('show')
}

closeMenu.onclick = function() {
    navMobile.classList.remove('show')
    overlay.classList.remove('show')
}