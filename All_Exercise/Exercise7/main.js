const carts = document.querySelectorAll('.cart')
const number = document.querySelector('.number')
const allRead = document.querySelector('.all-read')

const unread = document.querySelectorAll('.unread')
number.innerHTML = unread.length

carts.forEach(function(item) {
    item.onclick = function() {
        item.classList.remove('unread')
        const unread = document.querySelectorAll('.unread')
        number.innerHTML = unread.length
        if (unread.length === 0) {
            number.classList.add('hide')
        }
    }
})

allRead.onclick = function() {
    carts.forEach(function(item) {
        item.classList.remove('unread')
    })
    const unread = document.querySelectorAll('.unread')
    number.innerHTML = unread.length
    if (unread.length === 0) {
        number.classList.add('hide')
    }
}
